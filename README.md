Fontaine
===
[Fontaine](https://codeberg.org/portmaster/fontaine) is a command-line utility written a few years ago
for the Open Font Library project. Fontaine displays key meta
information about font files, including but not limited to font
name, style, weight, glyph count, character count, copyright,
license information and orthographic coverage.

One of the most useful aspects of this utility is its ability
to tell you what is included and what is missing from a given
font. This is especially useful for scholars, graphic designers
and anyone who needs to know about the specific coverage of a
font before they decide to use it in their projects. Read more
about Fontaine here.
